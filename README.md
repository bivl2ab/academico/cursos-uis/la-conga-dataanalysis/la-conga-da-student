# INTRODUCCIÓN AL ANÁLISIS DE DATOS. LA-CoNGA

## Bienvenidos  a análisis de datos 2023!!


<img src="https://gitlab.com/bivl2ab/academico/cursos-uis/la-conga-dataanalysis/la-conga-da-student/-/raw/master/imgs/banner_DA.png"  width="1000px" height="200px">


## Colaboratory (Google)

Vamos a utilizar la plataforma de google para editar, compartir y correr notebooks: [**Colaboratory**](https://colab.research.google.com/notebooks/welcome.ipynb)

- Necesitas una cuenta de gmail y luego entras a drive
- Colaboratory es un entorno de notebook de Jupyter gratuito que no requiere configuración y se ejecuta completamente en la nube.
    - Usaremos parte de la infraestructura de computo de google... gratis! (máximo procesos de 8 horas)
- Con Colaboratory, puedes escribir y ejecutar código, guardar y compartir análisis, y acceder a recursos informáticos potentes, todo gratis en tu navegador.
- También puedes usar los recursos de computador Local.


## Talleres (Problemsets)

Los talleres pretenden ser una herramienta practica para afianzar los conocimientos desarrollados durante las clases. En general se presentan como un conjunto de ejercicios que serán desarrollados **individualmente** por los estudiantes. Cada taller esta escrito como un notebook para la validación automática. Se pueden hacer tantos intentos como se quieran y unicamente la última respuesta será tomada en cuenta. Cada uno de los talleres se desarrollará  dentro de las fechas establecidas en el cronograma.



## Calendario y plazos

                        SESSION 1            SESSION 2              SESSION SATURDAY


     W01 Ene24-Ene26    Intro-colab-repos    Python-general
     W02 Ene31-Feb02    Python-Numpy         Pandas
     W03 Feb07-Feb09    Pandas               Visualización
     W04 Feb14-Feb16    análisis datos       análisis de datos
